all: data_pl
	@echo Doing all...

data_pl: data/pl_wykaz_opp.xml data/pl_finanse_wykaz_1procent.xml
	@echo Doing data_pl...

data/pl_wykaz_opp.xml: src/generator/pl_wykaz_opp.py download/pl_wykaz_opp_2017.xlsx
	python3 src/generator/pl_wykaz_opp.py \
		--input download/pl_wykaz_opp_2017.xlsx \
		--output data/pl_wykaz_opp.xml

data/pl_finanse_wykaz_1procent.xml: src/generator/pl_finanse_wykaz_1procent.py download/pl_finanse_wykaz_1procent_opp_2019_lista_podatnikow.xlsx download/pl_finanse_wykaz_1procent_opp_2019_wykaz_opp.xlsx
	python3 src/generator/pl_finanse_wykaz_1procent.py \
		--input1 download/pl_finanse_wykaz_1procent_opp_2019_wykaz_opp.xlsx \
		--input2 download/pl_finanse_wykaz_1procent_opp_2019_lista_podatnikow.xlsx \
		--source "https://www.gov.pl/web/finanse/1-procent-podatku-dla-opp" \
		--copyright "Ministerstwo Finansów (Polska)" \
		--license "Creative Commons Uznanie Autorstwa 3.0 Polska https://creativecommons.org/licenses/by/3.0/pl" \
		--output data/pl_finanse_wykaz_1procent.xml

download/pl_wykaz_opp_2017.xlsx:
	# 6ef73d9acfa5988a78376abe15ddd7fe37246703
	wget https://api.dane.gov.pl/media/resources/20150519/wykaz_organizacji_pozytku_publicznego_uprawnionych_do_1_w_II_2017_r.xlsx -O download/pl_wykaz_opp_2017.xlsx

download/pl_finanse_wykaz_1procent_opp_2019_lista_podatnikow.xlsx:
	# 1284697b583a09ef8413a8a44af01a1e39bb25dd
	wget https://www.gov.pl/attachment/cf259fab-8136-4709-92ef-46b3d54ae99a -O download/pl_finanse_wykaz_1procent_opp_2019_lista_podatnikow.xlsx

download/pl_finanse_wykaz_1procent_opp_2019_wykaz_opp.xlsx:
	# d987dd3fd6405498bfe9c0ab4d10bb27ea7dcb65
	wget https://www.gov.pl/attachment/9e1f2cea-e9b1-499a-bf87-44e23a59138c -O download/pl_finanse_wykaz_1procent_opp_2019_wykaz_opp.xlsx

clean:
	rm -rf download/ data/
	mkdir -p download data
