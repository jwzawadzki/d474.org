import openpyxl
import sys
from lxml import etree

class Arguments:
    def __init__(self, argv):

        i = 1
        while (i < len(argv)):
            if (argv[i] == "--input1"):
                self.input1 = argv[i + 1]
                i += 2;
            elif (argv[i] == "--input2"):
                self.input2 = argv[i + 1]
                i += 2;
            elif (argv[i] == "--source"):
                self.source = argv[i + 1]
                i += 2;
            elif (argv[i] == "--copyright"):
                self.copyright = argv[i + 1]
                i += 2;
            elif (argv[i] == "--license"):
                self.license = argv[i + 1]
                i += 2;
            elif (argv[i] == "--output"):
                self.output = argv[i + 1]
                i += 2;
            else:
                print(argv[i])
                sys.exit(1)

class Item:
    def __init__(self):
        self.lpodatnikow = float('nan')

args = Arguments(sys.argv)
root = etree.Element('data', xmlns="https://d474.org/pl_pit_1proc_opp")

child = etree.Element("info")
subchild = etree.Element('source')
subchild.text = args.source
child.append(subchild)
subchild = etree.Element('copyright')
subchild.text = args.copyright
child.append(subchild)
subchild = etree.Element('license')
subchild.text = args.license
child.append(subchild)
root.append(child)

lpodatnikow = { }
doc = openpyxl.load_workbook(args.input2)
for row in doc.active.iter_rows(min_row=4):
    # Liczba podatników w tys.
    krs = row[0].value
    lpodatnikow[ krs ] = Item()
    lpodatnikow[ krs ].lpodatnikow = row[2].value * 1000

doc = openpyxl.load_workbook(args.input1)
for row in doc.active.iter_rows(min_row=4, max_row=8872):
    child = etree.Element('c')

    # Numer KRS
    subchild = etree.Element('k')
    subchild.text = row[1].value.rstrip()
    child.append(subchild)

    # Nazwa organizacji pożytku publicznego
    subchild = etree.Element('n')
    subchild.text = row[2].value.rstrip()
    child.append(subchild)

    # Kwota w zł
    subchild = etree.Element('z')
    subchild.text = str(row[3].value)
    child.append(subchild)

    if lpodatnikow.get(row[1].value):
        subchild = etree.Element('l')
        subchild.text = str(lpodatnikow [ row[1].value ].lpodatnikow)
        child.append(subchild)

    root.append(child)

outFile = open(args.output, 'wb')
etree.indent(root, space="")
et = etree.ElementTree(root)
et.write(outFile, encoding="utf-8", pretty_print=True)
