import argparse
import openpyxl
from lxml import etree

parser = argparse.ArgumentParser()
parser.add_argument('--input')
parser.add_argument('--output')
args = parser.parse_args()

root = etree.Element('data', xmlns="https://d474.org/pl_wykaz_opp")

doc = openpyxl.load_workbook(args.input)

for row in doc.active.iter_rows(min_row=3):
    child = etree.Element('c')

    # NR KR
    subchild = etree.Element('k')
    subchild.text = row[1].value
    child.append(subchild)

    # NUMER_NIP
    subchild = etree.Element('i')
    subchild.text = str(row[2].value)
    child.append(subchild)

    # NAZWA
    subchild = etree.Element('n')
    subchild.text = row[3].value
    child.append(subchild)

    # WOJEWODZTWO
    subchild = etree.Element('w')
    subchild.text = row[4].value
    child.append(subchild)

    # POWIAT
    subchild = etree.Element('p')
    subchild.text = row[5].value
    child.append(subchild)

    # GMINA
    subchild = etree.Element('g')
    subchild.text = row[6].value
    child.append(subchild)

    # MIEJSCOWOSC
    subchild = etree.Element('m')
    subchild.text = row[7].value
    child.append(subchild)

    root.append(child)


outFile = open(args.output, 'wb')
etree.indent(root, space="")
et = etree.ElementTree(root)
et.write(outFile, encoding="utf-8", pretty_print=True)
